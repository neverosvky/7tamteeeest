const fetch = require('isomorphic-unfetch');

module.exports = {
    distDir: "../../dist/client",
    exportTrailingSlash: true,
};