import Link from 'next/link';
import React from 'react';

const Home = () => {
    return (
        <React.Fragment>
            <h1>Home</h1>
            <Link href="about"><a>About</a></Link>
        </React.Fragment>
    )
}

export default Home